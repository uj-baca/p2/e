#ifndef ZOMBIE_H
#define ZOMBIE_H

#include "Potwor.h"

class Zombie : public Potwor {
public:
    Zombie();

    Zombie(int);

    Zombie(int, Rasa);

    Zombie(int, Ranga);

    Zombie(Rasa, Ranga);

    Zombie(int, Rasa, Ranga);
};

#endif
