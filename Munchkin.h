#ifndef MUNCHKIN_H
#define MUNCHKIN_H

class Munchkin {
public:
    Munchkin();

    Munchkin(int);

    int pobierzPoziom();

    void ustawPoziom(int);

    bool pobierzSkarb();

    void ustawSkarb(bool);

private:
    int poziom;
    bool skarb;
};

#endif
