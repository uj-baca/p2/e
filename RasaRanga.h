#ifndef RASA_ENUM
#define RASA_ENUM

#include <iostream>

using namespace std;

enum Rasa {
    bezRasy,
    maly,
    wielki
};

enum Ranga {
    bezRangi,
    dostojny,
    swiatobliwy
};
#endif
