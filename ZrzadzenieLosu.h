#ifndef ZRZADZENIELOSU_H
#define ZRZADZENIELOSU_H

#include "Munchkin.h"
#include "ZombieWSkarpetkachSandalach.h"

class ZrzadzenieLosu {
public:
    ZrzadzenieLosu(Munchkin &, Potwor &, Rasa, Ranga, int, int, bool);

    void ustawPoziomMunchkina();

    void ustawPoziomPotwora();

    void ustawRangePotwora();

    void ustawRasePotwora();

    void ustawSkarb();

private:
    Munchkin &munchkin;
    Potwor &potwor;
    Rasa rasa;
    Ranga ranga;
    int poziom_munchkina;
    int poziom_potwora;
    bool skarb;
};

#endif
