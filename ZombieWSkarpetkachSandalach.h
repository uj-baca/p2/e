#ifndef ZOMBIEWSKARPETKACHSANDALACH_H
#define ZOMBIEWSKARPETKACHSANDALACH_H

#include "ZombieWSkarpetkach.h"
#include "ZombieWSandalach.h"

class Zombie_w_skarpetkach_sandalach : public Zombie_w_skarpetkach, public Zombie_w_sandalach {
public:
    Zombie_w_skarpetkach_sandalach();

    Zombie_w_skarpetkach_sandalach(int);

    Zombie_w_skarpetkach_sandalach(int, Rasa);

    Zombie_w_skarpetkach_sandalach(int, Ranga);

    Zombie_w_skarpetkach_sandalach(Rasa, Ranga);

    Zombie_w_skarpetkach_sandalach(int, Rasa, Ranga);

    bool pobierzOstatniaWalka();

    void ustawOstatniaWalka(bool);

private:
    bool ostatniaWalka;
};

#endif
