#include "ZombieWSkarpetkachSandalach.h"

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach()
        : Zombie(), Zombie_w_skarpetkach(), Zombie_w_sandalach() {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach(int poziom) : Zombie(poziom),
                                                                             Zombie_w_skarpetkach(poziom),
                                                                             Zombie_w_sandalach(poziom) {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach(int poziom, Rasa rasa) : Zombie(poziom, rasa),
                                                                                        Zombie_w_skarpetkach(poziom,
                                                                                                             rasa),
                                                                                        Zombie_w_sandalach(poziom,
                                                                                                           rasa) {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach(int poziom, Ranga ranga) : Zombie(poziom, ranga),
                                                                                          Zombie_w_skarpetkach(poziom,
                                                                                                               ranga),
                                                                                          Zombie_w_sandalach(poziom,
                                                                                                             ranga) {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach(Rasa rasa, Ranga ranga) : Zombie(rasa, ranga),
                                                                                         Zombie_w_skarpetkach(rasa,
                                                                                                              ranga),
                                                                                         Zombie_w_sandalach(rasa,
                                                                                                            ranga) {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

Zombie_w_skarpetkach_sandalach::Zombie_w_skarpetkach_sandalach(int poziom, Rasa rasa, Ranga ranga) : Zombie(poziom,
                                                                                                            rasa,
                                                                                                            ranga),
                                                                                                     Zombie_w_skarpetkach(
                                                                                                             poziom,
                                                                                                             rasa,
                                                                                                             ranga),
                                                                                                     Zombie_w_sandalach(
                                                                                                             poziom,
                                                                                                             rasa,
                                                                                                             ranga) {
    cout << "Jestem zombie w skarpetkach i sandalach" << endl;
    ustawOstatniaWalka(false);
}

bool Zombie_w_skarpetkach_sandalach::pobierzOstatniaWalka() {
    return ostatniaWalka;
}

void Zombie_w_skarpetkach_sandalach::ustawOstatniaWalka(bool ostatniaWalka) {
    this->ostatniaWalka = ostatniaWalka;
}