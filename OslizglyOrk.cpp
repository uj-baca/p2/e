#include "OslizglyOrk.h"

Oslizgly_Ork::Oslizgly_Ork() : Ork() {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

Oslizgly_Ork::Oslizgly_Ork(int poziom) : Ork(poziom) {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

Oslizgly_Ork::Oslizgly_Ork(int poziom, Rasa rasa) : Ork(poziom, rasa) {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

Oslizgly_Ork::Oslizgly_Ork(int poziom, Ranga ranga) : Ork(poziom, ranga) {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

Oslizgly_Ork::Oslizgly_Ork(Rasa rasa, Ranga ranga) : Ork(rasa, ranga) {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

Oslizgly_Ork::Oslizgly_Ork(int poziom, Rasa rasa, Ranga ranga) : Ork(poziom, rasa, ranga) {
    cout << "Jestem oslizglym orkiem" << endl;
    skarb = true;
}

bool Oslizgly_Ork::pobierzSkarb() {
    return skarb;
}

void Oslizgly_Ork::ustawSkarb(bool skarb) {
    this->skarb = skarb;
}
