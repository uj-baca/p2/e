#include "ZombieWSkarpetkach.h"

Zombie_w_skarpetkach::Zombie_w_skarpetkach() : Zombie() {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

Zombie_w_skarpetkach::Zombie_w_skarpetkach(int poziom) : Zombie(poziom) {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

Zombie_w_skarpetkach::Zombie_w_skarpetkach(int poziom, Rasa rasa) : Zombie(poziom, rasa) {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

Zombie_w_skarpetkach::Zombie_w_skarpetkach(int poziom, Ranga ranga) : Zombie(poziom, ranga) {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

Zombie_w_skarpetkach::Zombie_w_skarpetkach(Rasa rasa, Ranga ranga) : Zombie(rasa, ranga) {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

Zombie_w_skarpetkach::Zombie_w_skarpetkach(int poziom, Rasa rasa, Ranga ranga) : Zombie(poziom, rasa, ranga) {
    cout << "Jestem zombie w skarpetkach" << endl;
    skarpetki = true;
}

bool Zombie_w_skarpetkach::pobierzSkarpetki() {
    return skarpetki;
}

void Zombie_w_skarpetkach::ustawSkarpetki(bool skarpetki) {
    this->skarpetki = skarpetki;
}
