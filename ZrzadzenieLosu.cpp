#include "ZrzadzenieLosu.h"

ZrzadzenieLosu::ZrzadzenieLosu(Munchkin &munchkin, Potwor &potwor, Rasa rasa, Ranga ranga, int poziom_munchkina,
                               int poziom_potwora, bool skarb) : munchkin(munchkin), potwor(potwor) {
    this->rasa = rasa;
    this->ranga = ranga;
    this->poziom_potwora = poziom_potwora;
    this->poziom_munchkina = poziom_munchkina;
    this->skarb = skarb;
}

void ZrzadzenieLosu::ustawPoziomMunchkina() {
    munchkin.ustawPoziom(munchkin.pobierzPoziom() + poziom_munchkina);
}

void ZrzadzenieLosu::ustawPoziomPotwora() {
    potwor.ustawPoziom(potwor.pobierzPoziom() + poziom_potwora);
}

void ZrzadzenieLosu::ustawRangePotwora() {
    potwor.ustawRange(ranga);
}

void ZrzadzenieLosu::ustawRasePotwora() {
    potwor.ustawRase(rasa);
}

void ZrzadzenieLosu::ustawSkarb() {
    munchkin.ustawSkarb(skarb);
}
