#ifndef ZOMBIEWSKARPETKACH_H
#define ZOMBIEWSKARPETKACH_H

#include "Zombie.h"

class Zombie_w_skarpetkach : virtual public Zombie {
public:
    Zombie_w_skarpetkach();

    Zombie_w_skarpetkach(int);

    Zombie_w_skarpetkach(int, Rasa);

    Zombie_w_skarpetkach(int, Ranga);

    Zombie_w_skarpetkach(Rasa, Ranga);

    Zombie_w_skarpetkach(int, Rasa, Ranga);

    bool pobierzSkarpetki();

    void ustawSkarpetki(bool);

private:
    bool skarpetki;
};

#endif
