#ifndef TURA_H
#define TURA_H

#include "ZombieWSkarpetkachSandalach.h"
#include "Munchkin.h"
#include "OslizglyOrk.h"

class Tura {
public:
    Tura(Zombie &, Zombie_w_sandalach &, Zombie_w_skarpetkach &, Zombie_w_skarpetkach_sandalach &, Ork &,
         Oslizgly_Ork &, Munchkin &);

    void walka();

private:
    Zombie &m_z;
    Zombie_w_sandalach &m_zws;
    Zombie_w_skarpetkach &m_zwsz;
    Zombie_w_skarpetkach_sandalach &m_zwssz;
    Ork &m_o;
    Oslizgly_Ork &m_oo;
    Munchkin &m_m;
};

#endif
