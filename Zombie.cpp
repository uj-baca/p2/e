#include "Zombie.h"

Zombie::Zombie() : Potwor() {
    cout << "Jestem zombie" << endl;
}

Zombie::Zombie(int poziom) : Potwor(poziom) {
    cout << "Jestem zombie" << endl;
}

Zombie::Zombie(int poziom, Rasa rasa) : Potwor(poziom, rasa) {
    cout << "Jestem zombie" << endl;
}

Zombie::Zombie(int poziom, Ranga ranga) : Potwor(poziom, ranga) {
    cout << "Jestem zombie" << endl;
}

Zombie::Zombie(Rasa rasa, Ranga ranga) : Potwor(rasa, ranga) {
    cout << "Jestem zombie" << endl;
}

Zombie::Zombie(int poziom, Rasa rasa, Ranga ranga) : Potwor(poziom, rasa, ranga) {
    cout << "Jestem zombie" << endl;
}
