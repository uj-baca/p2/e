#ifndef ZOMBIEWSANDALACH_H
#define ZOMBIEWSANDALACH_H

#include "Zombie.h"

class Zombie_w_sandalach : virtual public Zombie {
public:
    Zombie_w_sandalach();

    Zombie_w_sandalach(int);

    Zombie_w_sandalach(int, Rasa);

    Zombie_w_sandalach(int, Ranga);

    Zombie_w_sandalach(Rasa, Ranga);

    Zombie_w_sandalach(int, Rasa, Ranga);

    bool pobierzSandaly();

    void ustawSandaly(bool);

private:
    bool sandaly;
};

#endif
