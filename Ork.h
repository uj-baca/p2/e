#ifndef ORK_H
#define ORK_H

#include "Potwor.h"

class Ork : public Potwor {
public:
    Ork();

    Ork(int);

    Ork(int, Rasa);

    Ork(int, Ranga);

    Ork(Rasa, Ranga);

    Ork(int, Rasa, Ranga);
};

#endif
