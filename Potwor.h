#ifndef POTWOR_H
#define POTWOR_H

#include "RasaRanga.h"

class Potwor {
public:
    Potwor();
    Potwor(int);
    Potwor(int, Rasa);
    Potwor(int, Ranga);
    Potwor(Rasa, Ranga);
    Potwor(int, Rasa, Ranga);
    int pobierzPoziom();
    Ranga pobierzRange();
    Rasa pobierzRase();
    void ustawPoziom(int);
    void ustawRase(Rasa);
    void ustawRange(Ranga);
private:
    int poziom;
    Rasa rasa;
    Ranga ranga;
};

#endif
