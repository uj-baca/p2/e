#include "Ork.h"

Ork::Ork() : Potwor() {
    cout << "Jestem orkiem" << endl;
}

Ork::Ork(int poziom) : Potwor(poziom) {
    cout << "Jestem orkiem" << endl;
}

Ork::Ork(int poziom, Rasa rasa) : Potwor(poziom, rasa) {
    cout << "Jestem orkiem" << endl;
}

Ork::Ork(int poziom, Ranga ranga) : Potwor(poziom, ranga) {
    cout << "Jestem orkiem" << endl;
}

Ork::Ork(Rasa rasa, Ranga ranga) : Potwor(rasa, ranga) {
    cout << "Jestem orkiem" << endl;
}

Ork::Ork(int poziom, Rasa rasa, Ranga ranga) : Potwor(poziom, rasa, ranga) {
    cout << "Jestem orkiem" << endl;
}
