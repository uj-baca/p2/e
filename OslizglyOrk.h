#ifndef OSLIZGLYORK_H
#define OSLIZGLYORK_H

#include "Ork.h"

class Oslizgly_Ork : public Ork {
public:
    Oslizgly_Ork();

    Oslizgly_Ork(int);

    Oslizgly_Ork(int, Rasa);

    Oslizgly_Ork(int, Ranga);

    Oslizgly_Ork(Rasa, Ranga);

    Oslizgly_Ork(int, Rasa, Ranga);

    void ustawSkarb(bool);

    bool pobierzSkarb();

private:
    bool skarb;
};

#endif
