#include "Potwor.h"

Potwor::Potwor():poziom(0), rasa(bezRasy), ranga(bezRangi) {
    cout<<"Jestem potworem"<<endl;
}

Potwor::Potwor(int poziom):poziom(poziom), rasa(bezRasy), ranga(bezRangi) {
    cout<<"Jestem potworem"<<endl;
}

Potwor::Potwor(int poziom, Rasa rasa):poziom(poziom), rasa(rasa), ranga(bezRangi) {
    cout<<"Jestem potworem"<<endl;
}

Potwor::Potwor(int poziom, Ranga ranga):poziom(poziom), rasa(bezRasy), ranga(ranga) {
    cout<<"Jestem potworem"<<endl;
}

Potwor::Potwor(Rasa rasa, Ranga ranga):poziom(0), rasa(rasa), ranga(ranga) {
    cout<<"Jestem potworem"<<endl;
}

Potwor::Potwor(int poziom, Rasa rasa, Ranga ranga):poziom(poziom), rasa(rasa), ranga(ranga) {
    cout<<"Jestem potworem"<<endl;
}

int Potwor::pobierzPoziom() {
    return poziom;
}

Ranga Potwor::pobierzRange() {
    return ranga;
}

Rasa Potwor::pobierzRase() {
    return rasa;
}

void Potwor::ustawPoziom(int poziom) {
    this->poziom = poziom;
}

void Potwor::ustawRase(Rasa rasa) {
    this->rasa = rasa;
}

void Potwor::ustawRange(Ranga ranga) {
    this->ranga = ranga;
}
