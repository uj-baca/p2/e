#include "ZombieWSandalach.h"

Zombie_w_sandalach::Zombie_w_sandalach() : Zombie() {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

Zombie_w_sandalach::Zombie_w_sandalach(int poziom) : Zombie(poziom) {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

Zombie_w_sandalach::Zombie_w_sandalach(int poziom, Rasa rasa) : Zombie(poziom, rasa) {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

Zombie_w_sandalach::Zombie_w_sandalach(int poziom, Ranga ranga) : Zombie(poziom, ranga) {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

Zombie_w_sandalach::Zombie_w_sandalach(Rasa rasa, Ranga ranga) : Zombie(rasa, ranga) {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

Zombie_w_sandalach::Zombie_w_sandalach(int poziom, Rasa rasa, Ranga ranga) : Zombie(poziom, rasa, ranga) {
    cout << "Jestem zombie w sandalach" << endl;
    sandaly = true;
}

bool Zombie_w_sandalach::pobierzSandaly() {
    return sandaly;
}

void Zombie_w_sandalach::ustawSandaly(bool sandaly) {
    this->sandaly = sandaly;
}
