#include <iostream>
#include "Tura.h"
#include "ZrzadzenieLosu.h"
#include "RasaRanga.h"

using namespace std;

int main() {
    Munchkin m;
    Zombie z;
    Zombie_w_sandalach z1;
    Zombie_w_skarpetkach z2;
    Zombie_w_skarpetkach_sandalach z3;
    Ork o;
    Oslizgly_Ork o1;
    ZrzadzenieLosu los(m, z1, bezRasy, bezRangi, 0, 0, true);
    los.ustawPoziomMunchkina();
    los.ustawPoziomPotwora();
    los.ustawRangePotwora();
    los.ustawRasePotwora();
    los.ustawSkarb();
    Tura t(z, z1, z2, z3, o, o1, m);
    t.walka();
    cout << m.pobierzPoziom() << endl;
    Tura t1(z, z1, z2, z3, o, o1, m);
    t1.walka();
    cout << m.pobierzPoziom() << endl;
    return 0;
}
