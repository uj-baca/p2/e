#include "Tura.h"

Tura::Tura(Zombie &z, Zombie_w_sandalach &zws, Zombie_w_skarpetkach &zwsz, Zombie_w_skarpetkach_sandalach &zwssz,
           Ork &o, Oslizgly_Ork &oo, Munchkin &m)
        : m_z(z), m_zws(zws), m_zwsz(zwsz), m_zwssz(zwssz), m_o(o), m_oo(oo), m_m(m) {}

void Tura::walka() {
    if (m_m.pobierzSkarb()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + 1);
    }
    int lvl = m_z.pobierzPoziom() + (m_z.pobierzRange() > 0 ? m_z.pobierzRange() + 1 : m_z.pobierzRange()) +
              m_z.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
    }

    bool sandaly = m_zws.pobierzSandaly();
    if (!sandaly) {
        m_zws.ustawPoziom(m_zws.pobierzPoziom() - 1);
    }
    lvl = m_zws.pobierzPoziom() + (m_zws.pobierzRange() > 0 ? m_zws.pobierzRange() + 1 : m_zws.pobierzRange()) +
          m_zws.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
        m_zws.ustawSandaly(true);
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
        m_zws.ustawSandaly(false);
    }
    if (!sandaly) {
        m_zws.ustawPoziom(m_zws.pobierzPoziom() + 1);
    }

    bool skarpetki = m_zwsz.pobierzSkarpetki();
    if (!skarpetki) {
        m_zwsz.ustawPoziom(m_zwsz.pobierzPoziom() - 1);
    }
    lvl = m_zwsz.pobierzPoziom() + (m_zwsz.pobierzRange() > 0 ? m_zwsz.pobierzRange() + 1 : m_zwsz.pobierzRange()) +
          m_zwsz.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
        m_zwsz.ustawSkarpetki(true);
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
        m_zwsz.ustawSkarpetki(false);
    }
    if (!skarpetki) {
        m_zwsz.ustawPoziom(m_zwsz.pobierzPoziom() + 1);
    }

    skarpetki = m_zwssz.pobierzSkarpetki();
    sandaly = m_zwssz.pobierzSandaly();
    if (!sandaly) {
        m_zwssz.ustawPoziom(m_zwssz.pobierzPoziom() - 1);
    }
    if (!skarpetki) {
        m_zwssz.ustawPoziom(m_zwssz.pobierzPoziom() - 1);
    }
    lvl = m_zwssz.pobierzPoziom() + (m_zwssz.pobierzRange() > 0 ? m_zwssz.pobierzRange() + 1 : m_zwssz.pobierzRange()) +
          m_zwssz.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
        if (!sandaly) {
            m_zwssz.ustawSandaly(true);
        } else {
            m_zwssz.ustawSkarpetki(true);
        }
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
        if (!sandaly) {
            m_zwssz.ustawSkarpetki(false);
        } else {
            m_zwssz.ustawSandaly(false);
        }
    }
    if (!sandaly) {
        m_zwssz.ustawPoziom(m_zwssz.pobierzPoziom() + 1);
    }
    if (!skarpetki) {
        m_zwssz.ustawPoziom(m_zwssz.pobierzPoziom() + 1);
    }

    lvl = m_o.pobierzPoziom() + (m_o.pobierzRange() > 0 ? m_o.pobierzRange() + 1 : m_o.pobierzRange()) +
          m_o.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
    }

    bool zbroja = m_oo.pobierzSkarb();
    if (!zbroja) {
        m_oo.ustawPoziom(m_oo.pobierzPoziom() - 1);
    }
    lvl = m_oo.pobierzPoziom() + (m_oo.pobierzRange() > 0 ? m_oo.pobierzRange() + 1 : m_oo.pobierzRange()) +
          m_oo.pobierzRase();
    if (lvl > m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() - (lvl < 1 ? 1 : lvl));
        m_m.ustawSkarb(false);
        m_oo.ustawSkarb(true);
    } else if (lvl < m_m.pobierzPoziom()) {
        m_m.ustawPoziom(m_m.pobierzPoziom() + (lvl < 1 ? 1 : lvl));
        m_m.ustawSkarb(true);
        m_oo.ustawSkarb(false);
    }
    if (!zbroja) {
        m_oo.ustawPoziom(m_oo.pobierzPoziom() + 1);
    }
}
