#include "Munchkin.h"

Munchkin::Munchkin() {
    this->poziom = 1;
    this->skarb = false;
}

Munchkin::Munchkin(int poziom) {
    this->poziom = poziom;
    this->skarb = false;
}

int Munchkin::pobierzPoziom() {
    return poziom;
}

void Munchkin::ustawPoziom(int poziom) {
    this->poziom = poziom;
}

bool Munchkin::pobierzSkarb() {
    return skarb;
}

void Munchkin::ustawSkarb(bool skarb) {
    this->skarb = skarb;
}
